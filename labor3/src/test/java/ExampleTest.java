import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pom.ExampleSignInPage;

public class ExampleTest {
    private static WebDriver webdriver;
    private ExampleSignInPage exampleSignInPage;

    @BeforeAll
    public static void setupTest() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webdriver = new ChromeDriver();
        /* Firefox Driver
        System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", true);
			driver = new FirefoxDriver(capabilities);
        */
        /* Opera Driver
        System.setProperty("webdriver.opera.driver", "src\\test\\java\\resources\\operadriver.exe");
		OperaOptions options = new OperaOptions();
		options.setBinary(
		new File("opera.exe"));
		driver = new OperaDriver(options);
        */
    }

    @BeforeEach
    public void navigateToURL() {
        webdriver.navigate().to("file:///C:/Users/ext_christina.domoko/Desktop/Lab2/example.html");
    }

    @Test
    public void sgnInTest() {
        WebElement usernameField = webdriver.findElement(By.id("uernameId"));
        usernameField.sendKeys("username");

        WebElement passwordField = webdriver.findElement(By.xpath("//input[contains(@id, 'passwordId')]"));
        passwordField.sendKeys("password");

        WebDriverWait wait = new WebDriverWait(webdriver, 10);
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.className("button")));
        loginButton.click();

        //assert methods can be used to verify if everything went right
    }

    @Test
    public void signInTestWithPOM() {
        exampleSignInPage = new ExampleSignInPage(webdriver);

        exampleSignInPage.writeTextToUsernameField("username");
        exampleSignInPage.writeTextToPasswordField("password");
        exampleSignInPage.pressSignInButton();
    }

    @AfterAll
    public static void quitDriver() {
        webdriver.quit();
    }
}
